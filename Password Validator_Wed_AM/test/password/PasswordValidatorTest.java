package password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * @author Hung Han Chen, 991531300
 * updated
 * 
 * */
public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCases() 
	{
		boolean result = PasswordValidator.hasValidCaseChar("aaAAAaA");
		assertTrue("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCasesExceptionBlank()
	{
		boolean result = PasswordValidator.hasValidCaseChar("");
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCasesExceptionNull()
	{
		boolean result = PasswordValidator.hasValidCaseChar(null);
		assertFalse("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCasesExceptionNumber()
	{
		boolean result = PasswordValidator.hasValidCaseChar("34324");
		assertFalse("Invalid case characters", result);
	}
	@Test
	public void testHasValidCasesBoundaryOutUpper()
	{
		boolean result = PasswordValidator.hasValidCaseChar("AAAAA");
		assertFalse("Invalid case characters", result);
	}
	@Test
	public void testHasValidCasesBoundaryOutLower()
	{
		boolean result = PasswordValidator.hasValidCaseChar("aaaaa");
		assertFalse("Invalid case characters", result);
	}
	@Test
	public void testHasValidCasesBoundaryIn()
	{
		boolean result = PasswordValidator.hasValidCaseChar("aA");
		assertTrue("Invalid case characters", result);
	}			
	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Invalid password lenght", PasswordValidator.isValidLength("1234567890"));
	}

	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid password lenght", PasswordValidator.isValidLength(null));
	}

	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Invalid password lenght", PasswordValidator.isValidLength("           "));
	}

	@Test
	public void testIsValidLengthExceptionBoundaryIn() {
		assertTrue("Invalid password lenght", PasswordValidator.isValidLength("12345678"));
	}

	@Test
	public void testIsValidLengthExceptionBoundaryOut() {
		assertFalse("Invalid password lenght", PasswordValidator.isValidLength("1234567"));
	}

	@Test
	public void testHasEnEnoughDigitsRegular() {
		assertTrue("Invalid password digits containing", PasswordValidator.hasEnEnoughDigits("12345"));
	}

	@Test
	public void testHasEnEnoughDigitsException() {
		assertFalse("Invalid password digits containing", PasswordValidator.hasEnEnoughDigits(null));
	}

	@Test
	public void testHasEnEnoughDigitsBoundaryIn() {
		assertTrue("Invalid password digits containing", PasswordValidator.hasEnEnoughDigits("12"));
	}

	@Test
	public void testHasEnEnoughDigitsBoundaryOut() {
		assertFalse("Invalid password digits containing", PasswordValidator.hasEnEnoughDigits("1"));
	}
}
